import matplotlib.pyplot as plt
import os

# Variables
STIA = 250  # Short term bonds minimum investment
STMT = 2  # Minimum term for short term bonds
STIR = 0.015  # Short term interest rate p.a.
LTIA = 1000  # Long term bonds minimum investment
LTMT = 5  # Minimum term for long term bonds
LTIR = 0.03  # Long term interest rate p.a.
n = 50  # Term of investment in nb of years


# Global function for investment return
def investreturn(term, initialamount, interestrate):
    ireturn = initialamount * (1 + interestrate) ** term
    return ireturn

# Calculation of investment return per year for 50 years
shorttermbondreturn = []
longtermbondreturn = []

STterm = [i for i in range(STMT, n)]
LTterm = [j for j in range(LTMT, n)]

for i in STterm:
    shorttermbondreturn.append(investreturn(i, STIA, STIR))

for j in LTterm:
    longtermbondreturn.append(investreturn(j, LTIA, LTIR))

# Figures plot
fig = plt.figure(1)
plt.plot(STterm, shorttermbondreturn, 'b-',
         LTterm, longtermbondreturn, 'ro-')
plt.xlabel('Term')
plt.ylabel('Investment return')
plt.title('Investment return on bonds')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath("../Results/InvestmentReturn.png"))
